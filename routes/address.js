const express = require('express');
const router = express.Router();
const Address = require('./models/address');





 router.post('/address', function (req, res) {
    Address.create({
            name : req.body.name,
            phone : req.body.phone,
            address : req.body.address,
            region : req.body.region,
            location : req.body.location
         }, 
        function (err, adres) {
            if (err) return res.status(500).send(err);
            res.status(200).send(adres);
        });
});

//post basing on user Id
router.post('/address1/:Phone', function (req, res, next) {

    Address.find({userPhone : req.params.Phone}, function (err, user) {
        if (err) return next(err);

        if(user.length <= 5) {
 
    Address.create({
          userPhone:  req.params.Phone,
          //addressMain: req.body.phone
          name : req.body.name,
             phone : req.body.phone,
            address : req.body.address,
            region : req.body.region,
            location : req.body.location
         }, 
        function (err, adres) {
            
             if (err) return res.status(500).send(err);
            res.json(200, {message: "Address added !"});
        });

    } else {
        res.json({message: "Maxmum number of address reached !"});
    }
    });

    });


//GET ALL
router.get('/address', function (req, res) {
    Address.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding orders.");
        res.status(200).send(users);
    });
});


//get based on current phone
router.get('/address/:phone', function (req, res) {
   
    Address.find({userPhone: req.params.phone.trim()}, function (err, adres){
            if(err){
               res.json(err.errors);
      }
   
      if(adres.length===0) {
         res.send("You got no address")
      }else {
       res.status(200);
       res.json(adres);
       
      }
         
    });
   });  
   

//get by ID
router.get('/address/:id', function (req, res) {
    Address.findById(req.params.id, function (err, adres) {
        if (err) return res.status(500).send("There was a problem finding order.");
        if (!adres) return res.status(404).send("No order found.");
        res.status(200).send(adres);
    });
});


// DELETES order by iD
router.delete('/address/:id', function (req, res) {
    Address.findByIdAndRemove(req.params.id, function (err, adres) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("Order removed succesfully");
    });
});

 


module.exports = router;
