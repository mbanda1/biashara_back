const express = require('express');
const app=express();
const path = require('path');
 

app.use(express.static(path.join(__dirname,'..','public')));

const categories = require('./categories');
const brands = require('./brands');
const station = require('./station');
const orders  = require('./orders');
const address = require('./address');
const users = require('./users');

app.use('',categories);
app.use('', brands);
app.use('', station);
app.use('', orders);
app.use('', address);
app.use('', users);

module.exports = app;